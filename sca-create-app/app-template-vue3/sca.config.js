const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  entries: {
    index: {
        entry: './src/main.js',
        template: './public/index.html',
        favicon: './public/favicon.ico',
        assets:''
    },
  },
  resolve: {
    alias: {
      
    },
  },
  setRules: (rules) => {
    rules.unshift({
        test: /\.css|scss$/,
        use: require.resolve("vue-style-loader")
      })
  },
    setPlugins: (plugins) => {
        plugins.push(
            new VueLoaderPlugin()
        )
    }
};
