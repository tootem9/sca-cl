#!/usr/bin/env node
'use strict';

//node 版本判断
const currentVersion = process.versions.node;
const requiredMajorVersion = parseInt(currentVersion.split(".")[0], 10);
const minimumMajorVersion = 10

if (requiredMajorVersion < minimumMajorVersion) {
  console.error(`Node.js 当前版本 ${currentVersion} 不支持的!`);
  console.error(`请使用 Node.js 版本，最低是 ${minimumMajorVersion} 或者更高其他版本`);
  process.exit(1);
}

require('./scaCreateApp');
