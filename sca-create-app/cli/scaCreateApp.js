/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-13 11:12:40
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-15 16:36:23
 */
const fs = require('fs');
const path = require('path');
const execa = require('execa');
const yargs = require('yargs-parser');
const {copy, removeSync} = require('fs-extra');
const colors = require('kleur');
const clear = require('clear')

//错误提示
const errorInfo = `${colors.red('[ERROR]')}`;


//打印错误信息
const errorLog = (msg) => {
    console.log(`${errorInfo} ${msg} `);
    process.exit(1);
}
const log = content => console.log(colors.green(content))


//判断依赖是否存在
const hasShellCommand = (commandLine) => {
    try {
        execa.commandSync(`${commandLine} --version`);
        return true;
    } catch (e) {
        return false;
    }
}

//npx sca-create-app new-dir --template @sca-cli/app-template-vue [ --target | --use-yarn | --use-npm | --no-install | --install | --cover
//shell/bash 命令操作语句校验
const validateArgs = (args) => {
    const { template, useYarn, useNpm, cover, target, install, _} = yargs(args);
    const toInstall = install !== undefined ? install : true;

    // console.log("template",template)
    // console.log("useYarn",useYarn)
    // console.log("useNpm",useNpm)
    // console.log("target",target)
    // console.log("_", _)
    
    if (useYarn && useNpm) {
        errorLog('不能同时使用 npm 和 yarn')
    }
    if (useYarn && !hasShellCommand('yarn')) {
        errorLog('yarn 没有安装过~ ')
    }
    if (useNpm && !hasShellCommand('npm')) {
        errorLog('npm 没有安装过~ ')
    }
    if (typeof template !== 'string') { 
        errorLog('--template 对应的模板文件丢失')
    }
    if (_.length > 3) {
        errorLog('命令行操作代码有误~请检查')
    }
    
    const targetDirectoryRelative = target || _[2];
    const targetDirectory = path.resolve(process.cwd(), targetDirectoryRelative)
    if (fs.existsSync(targetDirectory) && !cover) {
        errorLog(`${targetDirectory} 文件已经存在了，使用 \`--cover\` 覆盖这个目录`)
    }
    return {
        template,
        useYarn,
        useNpm,
        targetDirectoryRelative,
        targetDirectory,
        toInstall
    }
}

//模板文件版本判断
const versionProjectTemplate = async (template) => {
    let keywords;
    try {
        const {stdout} = await execa('npm', ['info', template, 'keywords', '--json']);
        keywords = JSON.parse(stdout);
        console.log("keywords",keywords);
    } catch (error) {
        console.log("error:",JSON.stringify(error));
        process.exit(1);
    }
}
//package.json 初始化写入
const initPackageFile = async (dir) => {
    const packageFile = path.join(dir, 'package.json')
    //
    removeSync(path.join(dir, 'package-lock.json'));
    removeSync(path.join(dir, 'node_modules'));

    const { scripts, dependencies, devDependencies } = require(packageFile)
    const { test, dev, build, ...otherScripts } = scripts
    const packjosnInfo = JSON.stringify({
            name: "sca-vue-app",
            author: "",
            license: "ISC",
            description:'sca-vue-app',
            scripts: { test, dev, build, ...otherScripts },
            dependencies,
            devDependencies
        },
        null,
        2,
    )
    await fs.promises.writeFile(packageFile, packjosnInfo)
    
    await fs.promises.writeFile(
        path.join(dir, '.gitignore'),
        ['dist', 'node_modules'].join('\n')
    )
}

const {template,useYarn, useNpm, toInstall, targetDirectoryRelative, targetDirectory} = validateArgs(
    process.argv,
);

let installer = 'npm';
if (useYarn) {
    installer = 'yarn'
}
//npx sca-create-app new-dir --template ./app-template-vue --no-install 本地测试安装文件
const localTemplate = template.startsWith('.'); 
const installTemplate = localTemplate?path.resolve(process.cwd(), template):path.join(targetDirectory, 'node_modules', template);

(async () => {
    //await versionProjectTemplate(installTemplate);
    clear()
    log(`  - 创建项目 ${targetDirectory}`)
    log(`\n  - 使用模板文件 ${colors.cyan(template)}`);
    log(`  - 创建新的项目 ${colors.cyan(targetDirectory)}\n`);

    //创建目录
    fs.mkdirSync(targetDirectory, { recursive: true });
    
    //创建package.json
    await fs.promises.writeFile(path.join(targetDirectory, 'package.json'), `{"name": "sca-vue-app"}`);
    
    //安装模板
    if(!localTemplate){
        try {
            await execa('npm', ['install', template, '--ignore-scripts'], {
                cwd: targetDirectory,
                all: true,
            });
        } catch (err) {
            console.error(err.all);
            throw err;
        }
    }
    
    //拷贝文件
    await copy(installTemplate, targetDirectory);
    
    //初始化 package.json
    await initPackageFile(targetDirectory);
    
    //自动安装依赖操作
    if (toInstall) {
        
        log(`  -初始化 install 安装项目依赖 \n`);

        const npmInstallOptions = {
            cwd: targetDirectory,
            stdio: 'inherit',
        };

        const installProcess = (packageManager) => {
            switch (packageManager) {
              case 'npm':
                return execa('npm', ['install', '--loglevel', 'error'], npmInstallOptions);
              case 'yarn':
                return execa('yarn', ['--silent'], npmInstallOptions);
              default:
                throw new Error('异常安装命令');
            }
        }
        const npmInstallProcess = installProcess(installer);
        npmInstallProcess.stdout && npmInstallProcess.stdout.pipe(process.stdout);
        npmInstallProcess.stderr && npmInstallProcess.stderr.pipe(process.stderr);
        await npmInstallProcess;
    } else {
        log(`  -执行"${installer} install 安装依赖"`);
    }

    //成功提示
    log(`  ======================== `)
    log(`  -cd ${targetDirectoryRelative}`);
    log(`  -${installer} start`);
    log(`  -${installer} run dev`);
    log(`  -${installer} run build`);
    log(`  -${installer} test`);
    log(`  ======================== `)
    
})()

