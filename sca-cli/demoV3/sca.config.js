const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  entries: {
    index: {
        entry: './src/main.js',
        template: './public/index.html',
        favicon: './public/favicon.ico',
        assets:'<script type="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>'
    },
  //   aboutNew: {
  //     entry: './about.js',
  //     template: './public/index.html',
  //     favicon: './public/favicon.ico',
  //     assets:''
  // },
  },
  resolve: {
    alias: {
      
    },
  },
  setRules: (rules) => {
    // rules.unshift(
    //     {
    //         test: /\.vue$/,
    //         use: require.resolve("vue-loader")
    //       }
    // )
    rules.unshift({
        test: /\.css|scss$/,
        use: require.resolve("vue-style-loader")
      })
  },
    setPlugins: (plugins) => {
        plugins.push(
          new VueLoaderPlugin()
        )
    }
};
