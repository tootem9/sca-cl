"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _yargsParser = _interopRequireDefault(require("yargs-parser"));

var _kleur = _interopRequireDefault(require("kleur"));

var _ora = _interopRequireDefault(require("ora"));

var _execa = _interopRequireDefault(require("execa"));

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:57:19
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 20:41:05
 */
// import webpack from 'webpack';
// import ora from 'ora';
// import rimraf from 'rimraf';
// import { buildConfig } from './config/webpackConfig';
// import { getWebpackConfig, getProjectPath } from './utils'  
//错误提示
var errorInfo = "".concat(_kleur["default"].red('[ERROR]')); //打印错误信息

var errorLog = function errorLog(msg) {
  var _context;

  console.log((0, _concat["default"])(_context = "".concat(errorInfo, " ")).call(_context, msg, " "));
  process.exit(1);
};

var log = function log(content) {
  return console.log(_kleur["default"].green(content));
};

var _default = /*#__PURE__*/(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
  var _yargs, mode, path, outDir, outFile, extensions, copyFiles, babelArgs, spinner, _yield$execa, stderr, exitCode;

  return _regenerator["default"].wrap(function _callee$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _yargs = (0, _yargsParser["default"])(process.argv), mode = _yargs.mode, path = _yargs.path, outDir = _yargs.outDir, outFile = _yargs.outFile, extensions = _yargs.extensions, copyFiles = _yargs.copyFiles; //命令校验

          if (!mode) {
            errorLog('--mode/-m 没有具体指定');
          }

          if (!path) {
            errorLog('--path/-p 没有具体指定');
          }

          if (!path) {
            errorLog('--path/-p 没有具体指定');
          }

          if (!outDir && !outFile) {
            if (!outDir) {
              errorLog('--out-dir/-d 没有具体指定');
            }

            if (!outFile) {
              errorLog('--out-file/-o 没有具体指定');
            }
          } // console.log("yargs(process.argv)",yargs(process.argv))
          // console.log("yargs(process.argv)",extensions)
          // babel 配置
          // 初始化配置


          babelArgs = [//require.resolve('@babel/cli/bin/babel'),
          'babel', path, '--extensions', extensions ? extensions : '.ts,.tsx,.js,jsx', '--ignore', '**/*.d.ts', '--config-file', require.resolve("./config/".concat(mode))]; //添加配置

          if (copyFiles) {
            babelArgs.push('--copy-files');
          }

          if (outDir) {
            babelArgs.push('--out-dir', outDir);
          }

          if (outFile) {
            babelArgs.push('--out-file', outFile);
          }

          spinner = (0, _ora["default"])('compiled for production...\n');
          spinner.start();
          _context2.prev = 11;
          _context2.next = 14;
          return (0, _execa["default"])('npx', babelArgs);

        case 14:
          _yield$execa = _context2.sent;
          stderr = _yield$execa.stderr;
          exitCode = _yield$execa.exitCode;

          if (exitCode !== 0) {
            spinner.stop();
            log("  ======================== ");
            log("  -Compiled successfully!");
            log("  ======================== ");
            log("\n");
            process.stderr.write(stderr);
            process.exit(0);
          } else {
            spinner.stop();
            log("  ======================== ");
            log("  -Compiled failed!");
            log("  ======================== ");
            log("\n");
          }

          _context2.next = 25;
          break;

        case 20:
          _context2.prev = 20;
          _context2.t0 = _context2["catch"](11);
          spinner.stop();
          console.error(_context2.t0.all);
          throw _context2.t0;

        case 25:
        case "end":
          return _context2.stop();
      }
    }
  }, _callee, null, [[11, 20]]);
}));

exports["default"] = _default;