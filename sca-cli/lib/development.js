"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _webpack = _interopRequireDefault(require("webpack"));

var _webpackDevServer = _interopRequireDefault(require("webpack-dev-server"));

var _webpackConfig = require("./config/webpackConfig");

var _utils = require("./utils");

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:51:31
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-08 09:55:32
 */
var _default = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
    var _context2;

    var host, port, webpackConfig, compiler, devServerConfig, devServer;
    return _regenerator["default"].wrap(function _callee$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            host = _ref.host, port = _ref.port;
            // webpack 配置
            webpackConfig = (0, _utils.getWebpackConfig)(_webpackConfig.developmentWebpackConfig);
            compiler = (0, _webpack["default"])(webpackConfig); //console.log("webpackConfig",webpackConfig)
            // console.log("compiler", compiler)
            //配置 webpack-dev-server 

            devServerConfig = {
              publicPath: '/',
              compress: true,
              noInfo: true,
              inline: true,
              hot: true,
              open: true
            };
            devServer = new _webpackDevServer["default"](compiler, devServerConfig);
            devServer.listen(port, host, function (err) {
              var _context;

              if (err) {
                return console.error(err);
              }

              console.warn((0, _concat["default"])(_context = "http://".concat(host, ":")).call(_context, port, "\n"));
            });
            (0, _forEach["default"])(_context2 = ['SIGINT', 'SIGTERM']).call(_context2, function (sig) {
              process.on(sig, function () {
                devServer.close();
                process.exit();
              });
            });

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee);
  }));

  return function (_x) {
    return _ref2.apply(this, arguments);
  };
}();

exports["default"] = _default;