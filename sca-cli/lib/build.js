"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _webpack = _interopRequireDefault(require("webpack"));

var _ora = _interopRequireDefault(require("ora"));

var _rimraf = _interopRequireDefault(require("rimraf"));

var _webpackConfig = require("./config/webpackConfig");

var _utils = require("./utils");

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:57:19
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 17:54:19
 */
var _default = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(options) {
    var outDir, webpackConfig, path;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            outDir = options.outDir;
            console.log("outDir", outDir); // webpack 配置

            webpackConfig = (0, _utils.getWebpackConfig)(_webpackConfig.buildConfig);
            path = (0, _utils.getProjectPath)(outDir || 'dist');
            webpackConfig.output.path = path;
            (0, _rimraf["default"])(path, function () {
              var spinner = (0, _ora["default"])('build for production...');
              spinner.start();
              (0, _webpack["default"])(webpackConfig).run(function (err, stats) {
                spinner.stop();
                if (err) throw err;
                process.stdout.write(stats.toString({
                  colors: true,
                  modules: false,
                  children: false,
                  chunks: false,
                  chunkModules: false
                }) + '\n');
              });
            });

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();

exports["default"] = _default;