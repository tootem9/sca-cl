"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports.buildConfig = exports.developmentWebpackConfig = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _semver = _interopRequireDefault(require("semver"));

var _terserWebpackPlugin = _interopRequireDefault(require("terser-webpack-plugin"));

var _miniCssExtractPlugin = _interopRequireDefault(require("mini-css-extract-plugin"));

var _cssMinimizerWebpackPlugin = _interopRequireDefault(require("css-minimizer-webpack-plugin"));

var _webpackMerge = _interopRequireDefault(require("webpack-merge"));

var _webpackbar = _interopRequireDefault(require("webpackbar"));

var _webpack = _interopRequireDefault(require("webpack"));

var _babelLoaderConfig = _interopRequireDefault(require("./babelLoaderConfig"));

var _module = require("./module");

var _path = _interopRequireDefault(require("path"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context; _forEachInstanceProperty(_context = ownKeys(Object(source), true)).call(_context, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context2; _forEachInstanceProperty(_context2 = ownKeys(Object(source))).call(_context2, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var optimization = {
  minimizer: [new _terserWebpackPlugin["default"]({
    terserOptions: {
      format: {
        comments: false
      }
    },
    extractComments: false
  }), //   new OptimizeCSSAssetsPlugin({
  //     cssProcessorOptions: {
  //       parser: safePostCssParser,
  //     },
  //     cssProcessorPluginOptions: {
  //       preset: ['default', {
  //         reduceTransforms: false,
  //         discardComments: {
  //           removeAll: true,
  //         },
  //         calc: false,
  //       }],
  //     },
  //   }),
  new _cssMinimizerWebpackPlugin["default"]({
    minimizerOptions: {
      preset: ['default', {
        discardComments: {
          removeAll: true
        }
      }]
    }
  })]
};
var vue = (0, _module.loadModule)('vue', _path["default"].join(process.cwd(), __dirname)); // console.log("semver.major(vue.version)",semver.major(vue.version))

var config = {
  output: {
    filename: 'js/[name].[chunkhash:8].js',
    chunkFilename: 'js/[name].[chunkhash:8].js',
    publicPath: '/'
  },
  module: {
    rules: [{
      test: /\.vue$/,
      use: [{
        loader: vue && _semver["default"].major(vue.version) === 3 ? require.resolve("vue-loader-v16") : require.resolve("vue-loader")
      } // {
      //   loader: require.resolve("@vue/compiler-sfc")
      // }
      ]
    }, {
      test: /\.(js|jsx|ts|tsx)$/,
      exclude: /node_modules/,
      use: [{
        loader: require.resolve('babel-loader'),
        options: _babelLoaderConfig["default"] // options: {
        //   presets: ['@babel/preset-env'],
        //   plugins: ['@babel/plugin-transform-runtime']
        // }
        // options:{
        //   cacheDirectory: false
        // }

      }]
    }, {
      test: /\.(css|scss)$/,
      use: [{
        loader: _miniCssExtractPlugin["default"].loader,
        options: {
          esModule: false
        }
      }, // {
      //     loader: require.resolve('style-loader'),
      // },
      // {
      //     loader: require.resolve('vue-style-loader'),
      // },
      {
        loader: require.resolve('css-loader'),
        options: {
          modules: false
        }
      }, {
        loader: require.resolve('sass-loader'),
        options: {
          sourceMap: true,
          implementation: require('sass')
        }
      }, // {
      //   loader: require.resolve('postcss-loader'),
      // },
      {
        loader: require.resolve('postcss-loader'),
        options: {
          sourceMap: true,
          postcssOptions: {
            plugins: [//'postcss-flexbugs-fixes',
            ["postcss-preset-env", {
              autoprefixer: {
                grid: true
              },
              features: {
                'nesting-rules': true
              },
              browsers: 'last 2 versions',
              stage: 3
            }]]
          }
        }
      }]
    }, // {
    //   test: /\.(png|jpg|jpeg|gif|svg|ico)$/,
    //   exclude: /node_modules/,
    //   use: [
    //     {
    //       loader: require.resolve('url-loader'),
    //       options: {
    //         limit: 1,
    //         name: 'images/[name].[hash:8].[ext]',
    //       },
    //     },
    //   ],
    // },
    {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      use: {
        loader: "file-loader",
        options: {
          outputPath: "font"
        }
      }
    }, {
      test: /\.(png|svg|jpg|gif)$/,
      use: {
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "img"
        }
      }
    }]
  },
  resolve: {
    extensions: [' ', '.ts', '.tsx', '.js', '.jsx', '.scss', '.svg'],
    alias: {}
  },
  plugins: [new _webpackbar["default"]()]
};
var buildConfig = (0, _webpackMerge["default"])({}, config, {
  mode: 'production',
  devtool: 'nosources-source-map',
  output: {
    filename: 'js/[name].[chunkhash:8].js',
    chunkFilename: 'js/[name].[chunkhash:8].js',
    publicPath: './'
  },
  optimization: _objectSpread(_objectSpread({}, optimization), {}, {
    splitChunks: {
      chunks: 'async',
      minSize: 20000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      automaticNameDelimiter: '~',
      enforceSizeThreshold: 50000,
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true
        },
        "default": {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    },
    runtimeChunk: {
      name: 'manifest'
    }
  }),
  plugins: [new _miniCssExtractPlugin["default"]({
    filename: 'stylesheet/[name].[contenthash:8].css',
    chunkFilename: 'stylesheet/[id].[contenthash:8].css'
  })]
});
exports.buildConfig = buildConfig;
var developmentWebpackConfig = (0, _webpackMerge["default"])({}, buildConfig, {
  mode: 'development',
  devtool: 'eval-cheap-source-map',
  output: {
    filename: 'js/[name].[chunkhash:8].js',
    chunkFilename: 'js/[name].[chunkhash:8].js',
    publicPath: '/'
  },
  optimization: {
    minimize: false
  },
  plugins: [//热更新
  new _webpack["default"].HotModuleReplacementPlugin()]
});
exports.developmentWebpackConfig = developmentWebpackConfig;