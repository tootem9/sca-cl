"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-18 17:39:34
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 17:39:37
 */
var _default = {
  presets: [require.resolve('@babel/preset-env'), require.resolve('@babel/preset-react'), require.resolve('@babel/preset-typescript')],
  plugins: [require.resolve('@babel/plugin-transform-runtime'), [require.resolve('@babel/plugin-proposal-decorators'), {
    legacy: true
  }], [require.resolve('@babel/plugin-proposal-class-properties'), {
    loose: true
  }], require.resolve('@babel/plugin-proposal-optional-chaining')]
};
exports["default"] = _default;