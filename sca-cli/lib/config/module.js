"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports.loadModule = exports.resolveModule = void 0;

var _module = _interopRequireDefault(require("module"));

var _path = _interopRequireDefault(require("path"));

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-28 17:21:24
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-28 17:29:11
 */
var createRequire = _module["default"].createRequire || _module["default"].createRequireFromPath || function (filename) {
  var mod = new _module["default"](filename, null);
  mod.filename = filename;
  mod.paths = _module["default"]._nodeModulePaths(_path["default"].dirname(filename));

  mod._compile("module.exports = require;", filename);

  return mod.exports;
};

var resolveModule = function resolveModule(request, context) {
  var resolvedPath;

  try {
    try {
      resolvedPath = createRequire(_path["default"].resolve(context, 'package.json')).resolve(request);
    } catch (e) {
      resolvedPath = require.resolve(request, {
        paths: [context]
      });
    }
  } catch (e) {}

  return resolvedPath;
};

exports.resolveModule = resolveModule;

var loadModule = function loadModule(request, context) {
  try {
    return createRequire(_path["default"].resolve(context, 'package.json'))(request);
  } catch (e) {
    var resolvedPath = resolveModule(request, context);

    if (resolvedPath) {
      return require(resolvedPath);
    }
  }
};

exports.loadModule = loadModule;