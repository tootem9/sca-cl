"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _commander = _interopRequireDefault(require("commander"));

var _package = require("../package.json");

var _development = _interopRequireDefault(require("./development"));

var _build = _interopRequireDefault(require("./build"));

var _compiled = _interopRequireDefault(require("./compiled"));

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:30:24
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-28 17:07:02
 */
_commander["default"].version(_package.version);

_commander["default"].command('dev').description('运行开发/测试环境').option('-h,--host <host>', '站点主机地址', 'localhost').option('-p,--port <port>', '站点端口号', 3000).action(_development["default"]);

_commander["default"].command('build').description('部署文件').option('-d,--out-dir <path>', '输出目录', 'dist').action(_build["default"]);

_commander["default"].command('compiled').description('打包编译操作').option('-m, --mode <es|lib>', '选择打包模式').option('-p,--path <path>', '源文件目录').option('-o,--out-file <outFile>', '输出文件名').option('-d,--out-dir <outDir>', '输出文件目录', 'dist').option('-ext, --extensions <extensions>', '要匹配的文件格式', '.ts,.tsx,.js,jsx').option('-c, --copy-files', '拷贝不参与编译的文件').action(_compiled["default"]);

_commander["default"].parse(process.argv);

if (!_commander["default"].args[0]) {
  _commander["default"].help();
}