"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports.getProjectPath = exports.getWebpackConfig = exports.getPathConfigJsFile = void 0;

var _keys = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/keys"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _entries = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/entries"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/objectWithoutProperties"));

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _webpackMerge = _interopRequireDefault(require("webpack-merge"));

var _htmlWebpackPlugin = _interopRequireDefault(require("html-webpack-plugin"));

/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-05 10:01:39
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-07 16:42:38
 */
// 获取项目配置文件 sca.config.js
var getPathConfigJsFile = function getPathConfigJsFile() {
  var configJsFileName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'sca.config.js';

  var configJsFilePath = _path["default"].join(process.cwd(), configJsFileName);

  if (_fs["default"].existsSync(configJsFilePath)) {
    return require(configJsFilePath);
  }

  return {};
}; // 编辑webpack 配置文件


exports.getPathConfigJsFile = getPathConfigJsFile;

var getWebpackConfig = function getWebpackConfig(config) {
  var _context;

  var _getPathConfigJsFile = getPathConfigJsFile(),
      entries = (0, _entries["default"])(_getPathConfigJsFile),
      setPluginsOptions = _getPathConfigJsFile.setPluginsOptions,
      banner = _getPathConfigJsFile.banner,
      setRules = _getPathConfigJsFile.setRules,
      setPlugins = _getPathConfigJsFile.setPlugins,
      webpackConfig = (0, _objectWithoutProperties2["default"])(_getPathConfigJsFile, ["entries", "setPluginsOptions", "banner", "setRules", "setPlugins"]);

  config.entry = {};
  setPluginsOptions && setPluginsOptions(config.module.rules[0].use[0].options);
  setRules && setRules(config.module.rules);
  setPlugins && setPlugins(config.plugins);
  (0, _forEach["default"])(_context = (0, _keys["default"])(entries || {})).call(_context, function (key) {
    if (entries[key].entry) {
      config.entry[key] = entries[key].entry;
    }

    config.plugins.push(new _htmlWebpackPlugin["default"]({
      template: entries[key].template,
      filename: "".concat(key, ".html"),
      chunks: ['manifest', key],
      favicon: entries[key].favicon || '',
      inject: entries[key].inject !== false,
      assets: entries[key].assets || ''
    }));
  });
  return (0, _webpackMerge["default"])(config, webpackConfig);
}; // 获取项目文件


exports.getWebpackConfig = getWebpackConfig;

var getProjectPath = function getProjectPath() {
  var dir = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : './';
  return _path["default"].join(process.cwd(), dir);
};

exports.getProjectPath = getProjectPath;