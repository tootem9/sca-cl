/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-07 17:14:48
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-07 17:20:08
 */
import Vue from 'vue'
import App from '/views/aboutNew.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
