module.exports = {
  "presets": [
    [
        "@babel/preset-env",
      {
        // loose false 更符合es6规范，代码量较多
        // loose true 更像es5代码，代码量较少
        // https://2ality.com/2015/12/babel6-loose-mode.html
          "loose": false,
        // 不做模块转化，直接用esm，用于tree-shaking
          "modules": false,
          "useBuiltIns": "usage",
          "corejs": 3,
          "targets": "Android >= 4.5, iOS >= 7"
          // "targets": {
          //   "browsers": ["> 1%", "last 2 versions", "not ie <= 8"]
          // }
      }
    ],
    //'@vue/cli-plugin-babel/preset',
  ],
  "plugins": [
    //"babel-plugin-transform-es2015-modules-commonjs",
    // [
    //   "@babel/plugin-transform-runtime",
    //   {
    //     "proposals": true,
    //     "corejs": 3
    //   }
    // ],
    [
      "component", {
        "libraryName": "bxs-ui-vue",
        "libDir": "lib",
        "style": "index.css"
      }
    ],
    //"@babel/plugin-transform-runtime",
    // ["import", 
    //   {
    //     "libraryName": "mint-ui",
    //     "style": true
    //   },
    //   'mint-ui'
    // ],
    // ["import", 
    //   // { "libraryName": "mint-ui", "libraryDirectory": 'lib', "style": "style.css" },
    //   {
    //     "libraryName": "bxs-ui-vue",
    //     "libraryDirectory": "lib",
    //     "style": "index.css"
    //   }
    //],
    ["import", {
      "libraryName": "vant",
      "libraryDirectory": 'lib',
      "style": true,
  }, "vant"],
//   ["import", 
//    {
//       "libraryName": "bxs-ui-vue",
//       "libraryDirectory": "lib",
//       "style": "index.css"
//     }
// ]
  ]
}
