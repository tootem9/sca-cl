# sca-cl

#### 介绍
项目打包脚手架工具


#### 安装教程

1.  sca-cli dev -h,--host <host: 站点主机地址 默认 localhost> -p,--port <port:站点端口号 默认 3000>
2.  sca-cli build -d,--out-dir <path: 部署输出文件目录 默认 dist>
3.  sca-cli compiled --mode <mode:es/lib 输入文件类型> ，--path <path: 打包文件目录>，--out-dir <out-dir: 输出文件目录 默认 dist> --out-file <out-file: 输出文件名称> ，--copy-files <copy-files: 拷贝不参与编译的文件> --extensions  <ext: 要匹配的文件格式 默认 .ts,.tsx,.js,.jsx> 
4.  项目文件的打包输入当前只支持es lib 格式 
#### 使用说明

1.  安装 npm i sca-cli -g
2.  开发 sca-cli dev
3.  部署 sca-cli build -d dist
3.  打包 sca-cli compiled --mode es/lib --path 打包文件 --out-dir 输出文件 --out-file 输入文件名 --copy-files  --extensions 要匹配的文件格式（build的命令行参数源于babel）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技


