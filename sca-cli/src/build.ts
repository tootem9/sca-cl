/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:57:19
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 17:54:19
 */
import webpack from 'webpack';
import ora from 'ora';
import rimraf from 'rimraf';
import { buildConfig } from './config/webpackConfig';
import { getWebpackConfig, getProjectPath } from './utils'  

export default async (options) => {
    const { outDir } = options
    console.log("outDir", outDir)
    // webpack 配置
    const webpackConfig = getWebpackConfig(buildConfig)
    const path = getProjectPath(outDir || 'dist')
    webpackConfig.output.path = path
    
    rimraf(path, () => {
        const spinner = ora('build for production...')
        spinner.start()
        webpack(webpackConfig).run((err, stats) => {
            spinner.stop()
            if (err) throw err
            process.stdout.write(stats.toString({
                colors: true,
                modules: false,
                children: false,
                chunks: false,
                chunkModules: false
                }) + '\n')
        });
    })
}
