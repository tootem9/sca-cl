/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:57:19
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 20:41:05
 */
// import webpack from 'webpack';
// import ora from 'ora';
// import rimraf from 'rimraf';
// import { buildConfig } from './config/webpackConfig';
// import { getWebpackConfig, getProjectPath } from './utils'  

import yargs from 'yargs-parser'
import colors from 'kleur'
import ora from 'ora';
import execa from 'execa';

//错误提示
const errorInfo = `${colors.red('[ERROR]')}`;

//打印错误信息
const errorLog = (msg) => {
    console.log(`${errorInfo} ${msg} `);
    process.exit(1);
}

const log = content => console.log(colors.green(content))


export default async () => {
    const { mode, path, outDir,outFile, extensions,copyFiles} = yargs(process.argv)
    //命令校验
    if (!mode) {
        errorLog('--mode/-m 没有具体指定')
    }
    if (!path) {
        errorLog('--path/-p 没有具体指定')
    }
    if (!path) {
        errorLog('--path/-p 没有具体指定')
    }
    
    if (!outDir && !outFile) {
        if (!outDir) {
            errorLog('--out-dir/-d 没有具体指定');
        }
        if (!outFile) {
            errorLog('--out-file/-o 没有具体指定');
        }
    }
    
    // console.log("yargs(process.argv)",yargs(process.argv))
    // console.log("yargs(process.argv)",extensions)
    
    // babel 配置
    // 初始化配置
    const babelArgs = [
        //require.resolve('@babel/cli/bin/babel'),
        'babel',
        path,
        '--extensions', extensions?extensions:'.ts,.tsx,.js,jsx',
        '--ignore', '**/*.d.ts',
        '--config-file', require.resolve(`./config/${mode}`),
    ];
    //添加配置
    if (copyFiles) {
        babelArgs.push('--copy-files')
    }
    if (outDir) {
        babelArgs.push('--out-dir',outDir)
    }
    if (outFile) {
        babelArgs.push('--out-file',outFile)
    }
    const spinner = ora('compiled for production...\n')
    spinner.start()

    try {
        const { stderr, exitCode } = await execa('npx', babelArgs);
        if (exitCode !== 0) {
            spinner.stop()
            log(`  ======================== `)
            log(`  -Compiled successfully!`);
            log(`  ======================== `)
            log(`\n`); 
            process.stderr.write( stderr );
            process.exit(0);
            
        } else {
            spinner.stop()
            log(`  ======================== `)
            log(`  -Compiled failed!`);
            log(`  ======================== `)
            log(`\n`); 
        }
    } catch (err) {
        spinner.stop()
        console.error(err.all);
        throw err;
    }


    
     
}
