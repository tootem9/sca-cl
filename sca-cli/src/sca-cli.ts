/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:30:24
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-28 17:07:02
 */
import program from 'commander';
import { version } from '../package.json';
import development from './development';
import build from './build';
import compiled from './compiled';

program.version(version)

program
    .command('dev')
    .description('运行开发/测试环境')
    .option('-h,--host <host>', '站点主机地址', 'localhost')
    .option('-p,--port <port>', '站点端口号', 3000)
    .action(development);

program
    .command('build')
    .description('部署文件')
    .option('-d,--out-dir <path>', '输出目录','dist')
    .action(build);

program
    .command('compiled')
    .description('打包编译操作')
    .option('-m, --mode <es|lib>', '选择打包模式')
    .option('-p,--path <path>', '源文件目录')
    .option('-o,--out-file <outFile>', '输出文件名')
    .option('-d,--out-dir <outDir>', '输出文件目录','dist')
    .option('-ext, --extensions <extensions>', '要匹配的文件格式', '.ts,.tsx,.js,jsx')
    .option('-c, --copy-files', '拷贝不参与编译的文件')
    .action(compiled);

program.parse(process.argv);

if (!program.args[0]) {
    program.help();
}
