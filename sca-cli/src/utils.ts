/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-05 10:01:39
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-07 16:42:38
 */
import path from 'path';
import fs from 'fs';
import { Configuration } from 'webpack';
import webpackMerge from 'webpack-merge';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export interface typeCustomConfig extends Configuration {
    entries?: object;
    banner?: string;
    setPluginsOptions?: (options) => void;
    setRules?: (rules) => void;
    setPlugins?: (plugins) => void;
}
// 获取项目配置文件 sca.config.js
const getPathConfigJsFile = (configJsFileName = 'sca.config.js'): typeCustomConfig => { 
    const configJsFilePath = path.join(process.cwd(), configJsFileName);
    if (fs.existsSync(configJsFilePath)) {
        return require(configJsFilePath)
    }
    return {}
}
// 编辑webpack 配置文件
const getWebpackConfig = (config: Configuration) => {
    const { entries, setPluginsOptions, banner, setRules, setPlugins, ...webpackConfig } = getPathConfigJsFile();
    
    config.entry = {};
    setPluginsOptions && setPluginsOptions(config.module.rules[0].use[0].options);
    setRules && setRules(config.module.rules);
    setPlugins && setPlugins(config.plugins);
    
    Object.keys(entries || {}).forEach((key) => {
        if (entries[key].entry) {
        config.entry[key] = entries[key].entry;
        }
        config.plugins.push(new HtmlWebpackPlugin({
            template: entries[key].template,
            filename: `${key}.html`,
            chunks: ['manifest', key],
            favicon: entries[key].favicon || '',
            inject: entries[key].inject !== false,
            assets: entries[key].assets || ''
        }));
    });
    
    return webpackMerge(config, webpackConfig);
}

// 获取项目文件
const getProjectPath = (dir = './'): string => {
    return path.join(process.cwd(), dir);
};
  

export {
    getPathConfigJsFile,
    getWebpackConfig,
    getProjectPath
}