/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-04 19:51:31
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-08 09:55:32
 */
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';

import { developmentWebpackConfig } from './config/webpackConfig';
import { getWebpackConfig } from './utils'  
export interface developmentType {
    host: string;
    port: number;
}
export default async ({ host, port }: developmentType) => {
   

    // webpack 配置
    const webpackConfig = getWebpackConfig(developmentWebpackConfig)
    const compiler = webpack(webpackConfig);

    //console.log("webpackConfig",webpackConfig)
    // console.log("compiler", compiler)
    
    
    //配置 webpack-dev-server 
    const devServerConfig = {
        publicPath: '/',
        compress: true,
        noInfo: true,
        inline: true,
        hot: true,
        open:true,
    }
    const devServer = new WebpackDevServer(compiler, devServerConfig);
    devServer.listen(port, host, (err) => {
        if (err) {
            return console.error(err);
        }
        console.warn(`http://${host}:${port}\n`);
    });
    
    ['SIGINT', 'SIGTERM'].forEach((sig: any) => {
        process.on(sig, () => {
          devServer.close();
          process.exit();
        });
    });
}
