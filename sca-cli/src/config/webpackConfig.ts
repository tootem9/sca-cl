/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-05 16:36:28
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-28 17:39:19
 */
import semver from 'semver';
import TerserPlugin from 'terser-webpack-plugin';
// import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
// import safePostCssParser from 'postcss-safe-parser';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';

import webpackMerge from 'webpack-merge';
import WebpackBar from 'webpackbar';
import webpack, { Configuration } from 'webpack';
import babelLoaderConfig from './babelLoaderConfig';
import { loadModule } from './module'
import path from 'path';

const optimization: Configuration['optimization'] = {
    minimizer: [
        new TerserPlugin({
            terserOptions: {
              format: {
                comments: false,
              },
            },
            extractComments: false,
        }),
    //   new OptimizeCSSAssetsPlugin({
    //     cssProcessorOptions: {
    //       parser: safePostCssParser,
    //     },
    //     cssProcessorPluginOptions: {
    //       preset: ['default', {
    //         reduceTransforms: false,
    //         discardComments: {
    //           removeAll: true,
    //         },
    //         calc: false,
    //       }],
    //     },
    //   }),
      new CssMinimizerPlugin({
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: { removeAll: true },
            },
          ],
        },
      }),
    ],
};
const vue = loadModule('vue', path.join(process.cwd(), __dirname))
// console.log("semver.major(vue.version)",semver.major(vue.version))
const config: Configuration = {
    output: {
      filename: 'js/[name].[chunkhash:8].js',
      chunkFilename: 'js/[name].[chunkhash:8].js',
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: [
            {
              loader: (vue && semver.major(vue.version) === 3)?require.resolve("vue-loader-v16"):require.resolve("vue-loader")
            },
            // {
            //   loader: require.resolve("@vue/compiler-sfc")
            // }
          ]
        },
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: require.resolve('babel-loader'),
              options: babelLoaderConfig,
              // options: {
              //   presets: ['@babel/preset-env'],
              //   plugins: ['@babel/plugin-transform-runtime']
              // }
              // options:{
              //   cacheDirectory: false
              // }
            },
          ],
        },
        {
          test: /\.(css|scss)$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                esModule: false,
              },
            },
            // {
            //     loader: require.resolve('style-loader'),
            // },
            // {
            //     loader: require.resolve('vue-style-loader'),
            // },
           
            {
              loader: require.resolve('css-loader'),
              options: {
                modules: false
              },
            },
            {
              loader: require.resolve('sass-loader'),
              options: {
                sourceMap: true,
                implementation: require('sass'),
              },
            },
            // {
            //   loader: require.resolve('postcss-loader'),
            // },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                sourceMap: true,
                postcssOptions: {
                  plugins: [
                    //'postcss-flexbugs-fixes',
                    [
                      "postcss-preset-env",
                      {
                        autoprefixer: { grid: true },
                        features: {
                          'nesting-rules': true
                        },
                        browsers: 'last 2 versions',
                        stage: 3,
                      },
                    ],
                    
                  ],
                }
              },
            },
          ],
        },
        // {
        //   test: /\.(png|jpg|jpeg|gif|svg|ico)$/,
        //   exclude: /node_modules/,
        //   use: [
        //     {
        //       loader: require.resolve('url-loader'),
        //       options: {
        //         limit: 1,
        //         name: 'images/[name].[hash:8].[ext]',
        //       },
        //     },
        //   ],
        // },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: {
            loader: "file-loader",
            options: {
              outputPath:"font"
            }
          }
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath:"img"
            }
          }
        },
      ],
    },
  
    resolve: {
      extensions: [' ', '.ts', '.tsx', '.js', '.jsx', '.scss', '.svg'],
      alias: {
        
      },
    },
    plugins: [
      new WebpackBar(),
    ],
  };
  
const buildConfig: Configuration = webpackMerge({}, config, {
    mode: 'production',
    devtool: 'nosources-source-map',
    output: {
      filename: 'js/[name].[chunkhash:8].js',
      chunkFilename: 'js/[name].[chunkhash:8].js',
      publicPath: './',
    },
    optimization: {
      ...optimization,
      splitChunks: {
        chunks: 'async',
        minSize: 20000,
        minRemainingSize: 0,
        minChunks: 1,
        maxAsyncRequests: 30,
        maxInitialRequests: 30,
        automaticNameDelimiter: '~',
        enforceSizeThreshold: 50000,
        cacheGroups: {
            defaultVendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
            reuseExistingChunk: true
            },
            default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
            }
        }
      },
      runtimeChunk: {
        name: 'manifest',
      },
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'stylesheet/[name].[contenthash:8].css',
        chunkFilename: 'stylesheet/[id].[contenthash:8].css',
      }),
    ],
});
const developmentWebpackConfig: Configuration = webpackMerge({}, buildConfig, {
    mode: 'development',
    devtool: 'eval-cheap-source-map',
    output: {
        filename: 'js/[name].[chunkhash:8].js',
        chunkFilename: 'js/[name].[chunkhash:8].js',
        publicPath: '/',
    },
    optimization: {
        minimize: false,
    },
    plugins: [
        //热更新
        new webpack.HotModuleReplacementPlugin(),
    ],
});

export {
  developmentWebpackConfig,
  buildConfig
}