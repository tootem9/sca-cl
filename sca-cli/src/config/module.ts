/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-28 17:21:24
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-28 17:29:11
 */

import Module from 'module';
import path from 'path';

const createRequire = Module.createRequire || Module.createRequireFromPath || function (filename: string) {
    const mod:any = new Module(filename, null)
    mod.filename = filename
    mod.paths = Module._nodeModulePaths(path.dirname(filename))
  
    mod._compile(`module.exports = require;`, filename)
  
    return mod.exports
}
  

const resolveModule = function (request:any, context:any) {
    let resolvedPath:any
    try {
      try {
        resolvedPath = createRequire(path.resolve(context, 'package.json')).resolve(request)
      } catch (e) {
        resolvedPath = require.resolve(request, { paths: [context] })
      }
    } catch (e) {}
    return resolvedPath
  }
  
const loadModule = function (request:any, context:any) {
    try {
      return createRequire(path.resolve(context, 'package.json'))(request)
    } catch (e) {
      const resolvedPath:any = resolveModule(request, context)
      if (resolvedPath) {
        return require(resolvedPath)
      }
    }
}


export {
  resolveModule,
  loadModule,

}