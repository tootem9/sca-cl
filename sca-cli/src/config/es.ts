/*
 * @Description: 
 * @version: 
 * @Author: nathan
 * @Date: 2021-01-18 17:39:34
 * @LastEditors: nathan
 * @LastEditTime: 2021-01-18 17:39:37
 */
export default {
  presets: [
    [require.resolve('@babel/preset-env'), {
      modules: false,
    }],
    require.resolve('@babel/preset-react'),
    require.resolve('@babel/preset-typescript'),
  ],
  plugins: [
    require.resolve('@babel/plugin-transform-runtime'),
    [require.resolve('@babel/plugin-proposal-decorators'), { legacy: true }],
    [require.resolve('@babel/plugin-proposal-class-properties'), { loose: true }],
    require.resolve('@babel/plugin-proposal-optional-chaining'),
  ],
};
