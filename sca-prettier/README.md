
# sca-prettier 配置

> prettier 模块配置

## Install

```sh

$ yarn add sca-prettier -D
$ npm install sca-prettier -S

```

## Usage

In package.json:

```json

{
  "prettier": "sca-prettier"
}

```

Or in .prettierrc.js:

```js

module.exports = require("sca-prettier");

```

 
